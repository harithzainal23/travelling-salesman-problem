import model.City;
import model.Route;
import utility.Distance;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class GreatDelugeApplication {

    int counter = 0;
    double maximumIteration = 30;

    //double rainSpeed;
    double waterLevel;

    double estimatedQuality = 20;

    Distance distance = new Distance();

    public static void main(String[] args) {
        //System.out.println("Hill Climbing");
        GreatDelugeApplication greatDelugeApplication = new GreatDelugeApplication();
        Route route = new Route(greatDelugeApplication.newCities);
        //System.out.println(route.getCityNames());
        Distance distance = new Distance();
        //ystem.out.println(distance.getTotalDistance(route));

        double initialTemperature = 99;
        greatDelugeApplication.findShortestRoute(route);
    }

    private ArrayList<City> newCities = new ArrayList<City>(Arrays.asList(
            new City("1", 36.902328, 5.865524),
            new City("2", 32.268564, -9.772170),
            new City("3", 38.430958, 4.616171) ,
            new City("4", 17.085076, -13.717743),
            new City("5", -2.378420, -12.912119),
            new City("6", 35.229322, -11.952753),
            new City("7", 8.734037, -37.925268),
            new City("8", 18.213856,30.067160),
            new City("9", -17.367236, 14.994152),
            new City("10", -16.055342, 36.092639),
            new City("11", 36.893950, -4.489910),
            new City("12", 11.354847, 37.879510),
            new City("13", 39.040952, 6.060413),
            new City("14", -7.949413 , 3.900184) ,
            new City("15", -35.113496 , -9.111273),
            new City("16", 3.920608, 28.751273),
            new City("17", 24.411612  ,26.044301),
            new City("18", 34.976862, 5.514005),
            new City("19", 12.745841, 28.464310 ),
            new City("20", 23.195509, 25.060627),
            new City("21", 9.107489, 16.970196),
            new City("22", 39.281107, 4.053225) ,
            new City("23", 9.510174, -15.065098),
            new City("24", 39.103806, -2.333589),
            new City("25", 3.162014, 37.917558),
            new City("26", -6.567441, -7.311222 ),
            new City("27", 8.555034,38.095727),
            new City("28", 0.938855, 11.778574),
            new City("29", -36.020211, -4.791656),
            new City("30", -3.040275, 20.917682)

    ));

    public Route findShortestRoute (Route currentRoute) {
        System.out.println("BEGIN GREAT DELUGE");
        System.out.print(currentRoute.getCityNames());
        waterLevel=distance.getTotalDistance(currentRoute);
        System.out.println("    "+String.format(" %.2f",distance.getTotalDistance(currentRoute)));
        Route bestRoute=null;

        Boolean loop = true;

        double rainSpeed=(waterLevel-estimatedQuality)/maximumIteration;


        System.out.println("RAIN SPEED IS: "+rainSpeed);
        while(counter<maximumIteration){
            System.out.println("#"+counter);
            counter++;
            Route newRoute = shuffledRoute(new Route(currentRoute));
            System.out.print(newRoute.getCityNames());
            System.out.print("    "+String.format(" %.2f",distance.getTotalDistance(newRoute)));
            System.out.print("    waterLevel: "+String.format(" %.2f",waterLevel));

            if(distance.getTotalDistance(newRoute)<waterLevel){

                currentRoute=newRoute;
                waterLevel = waterLevel - rainSpeed;

            }

            System.out.println("    "+String.format(" %.2f",distance.getTotalDistance(currentRoute)));


        }





        System.out.println("Best Solution:    "+String.format(" %.2f",distance.getTotalDistance(currentRoute)));

        return currentRoute;


    }

    private Route shuffledRoute(Route route) {

        int cityA = new Random().nextInt(route.getCities().size()); //randomly generate index number for a city
        int cityB = new Random().nextInt(route.getCities().size()); //randomly generate index number for a city

        while (cityA == cityB) {
            cityA = new Random().nextInt(route.getCities().size()); //randomly generate index number for a city
            cityB = new Random().nextInt(route.getCities().size()); //randomly generate index number for a city
        }

        //System.out.println(cityA);
        //System.out.println(cityB);

        Collections.swap(route.getCities(),cityA,cityB);

        return route;
    }


}
