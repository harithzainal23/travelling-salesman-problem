package model;

import utility.Distance;

import java.util.ArrayList;
import java.util.stream.IntStream;

public class Population {

    Distance distance;

    int populationSize=8;
    private ArrayList<Route> routes = new ArrayList<Route>();
    //public Population(int populationSize, GeneticAlgorithm geneticAlgorithm) {
    //    IntStream.range(0, populationSize).forEach(x -> routes.add(new Route(geneticAlgorithm.getInitialRoute())));
   // }
    public Population(int populationSize, ArrayList<City> cities) {
        IntStream.range(0, populationSize).forEach(x -> routes.add(new Route(cities)));
    }
    public ArrayList<Route> getRoutes() { return routes;}




}
