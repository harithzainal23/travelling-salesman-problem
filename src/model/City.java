package model;

public class City {

    private double longitude;
    private double latitude;
    private String name;

    public City( String name,double longitude, double latitude){

        this.longitude = longitude;
        this.latitude = latitude;
        this.name=name;
    }

    public double getLongitude(){return longitude;}
    public void setLongitude(double longitude){this.longitude = longitude;}

    public double getLatitude(){return latitude;}
    public void setLatitude(double latitude){this.latitude = latitude;}

    public String getName(){return name;}
    public void setName(String name){this.name = name;}




}
