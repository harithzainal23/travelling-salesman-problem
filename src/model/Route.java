package model;


import utility.Distance;

import java.util.ArrayList;

import java.util.Arrays;
import java.util.Collections;



public class Route {

    private ArrayList<City> cities = new ArrayList<City>();
    private boolean isFitnessChanged = true;
    private double fitness = 0;
    Distance distance ;

    public Route(ArrayList<City> cities){
        this.cities.addAll(cities);
        Collections.shuffle(this.cities);
    }
    public Route(Route route){ this.cities.addAll(route.cities); }


    public ArrayList<String> getCityNames(){

        ArrayList<String> cityNames = new ArrayList<>();

        for (int i=0; i<cities.size(); i++){

                cityNames.add(cities.get(i).getName());

        }
        return cityNames;
    }

    public ArrayList<City> getCities() {
     return cities;
    }




   /* public double getTotalDistance() {
        int citiesSize = this.cities.size();
        return this.cities.stream().mapToDouble(x -> {
            int cityIndex = this.cities.indexOf(x);
            double returnValue = 0;
            if (cityIndex < citiesSize - 1) returnValue = x.distance(this.cities.get(cityIndex + 1));
            return returnValue;
        }).sum() + this.cities.get(citiesSize - 1).(this.cities.get(0));
    }
    public String getTotalStringDistance() {
        String returnValue = String.format("%.2f",this.getTotalDistance());
        if (returnValue.length() == 7) returnValue = " "+returnValue;
        return returnValue;
    }
    */


}
