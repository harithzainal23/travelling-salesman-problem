package utility;

import model.City;
import model.Route;

public class Distance {

    public double distance(City cityA, City cityB){
        //System.out.println("CITY A : "+cityA.getName() );
        //System.out.println("CITY B : "+cityB.getName() );
        double differenceLongitude = cityA.getLongitude() - cityB.getLongitude();
        double differenceLatitude = cityA.getLatitude() - cityB.getLatitude();
        double a = Math.pow(Math.sin(differenceLatitude / 2D), 2D) +
                Math.cos(cityB.getLatitude()) * Math.cos(cityA.getLatitude()) * Math.pow(Math.sin(differenceLongitude / 2D), 2D);
        return   2D * Math.atan2(Math.sqrt(a), Math.sqrt(1D - a));

    }

    public double getTotalDistance(Route route){
        double totalDistance = 0;
        for(int i=0; i<route.getCities().size()-1; i++){
            totalDistance += distance(route.getCities().get(i),route.getCities().get(i+1));
            //System.out.println(totalDistance);
        }

        return totalDistance;
    }


}
