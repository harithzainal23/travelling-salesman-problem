import model.City;
import model.Population;
import model.Route;
import utility.Distance;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.IntStream;

public class GeneticAlgorithmApplication {


    double mutationRate = 0.25;
    int tournamentSize = 3;
    int eliteRouteNumber = 2;
    int generationNumber = 8;
    int populationSize = 8;

    public static void main(String[] args) {

        Route bestRoute = null;




        System.out.println("Genetic Algorithm");

        GeneticAlgorithmApplication geneticAlgorithmApplication = new GeneticAlgorithmApplication();

        Population population = new Population(geneticAlgorithmApplication.populationSize,geneticAlgorithmApplication.newCities );
        //population.sortRoutesByFitness();

        Route route = new Route(geneticAlgorithmApplication.newCities);
        System.out.println(route.getCityNames());
        Distance distance = new Distance();

        System.out.println(distance.getTotalDistance(route));
        //geneticAlgorithmApplication.findShortestRoute(route);

        int generationCounter=0;

        while (generationCounter < geneticAlgorithmApplication.generationNumber) {
            System.out.println("GENERATION: "+generationCounter);
            population = geneticAlgorithmApplication.evolve(population);
            generationCounter++;


            for(int i = 0; i < geneticAlgorithmApplication.populationSize;i++) {
               // System.out.println(i);
                //population.sortRoutesByFitness(population);

               // bestRoute = population.getRoutes().get(0);
                //System.out.println("Best Route Found so far: " + bestRoute.getCityNames());


                    //System.out.println(distance.getTotalDistance(population.getRoutes().get(i)));
                    //System.out.println(distance.getTotalDistance(population.getRoutes().get(i-1)));
                //System.out.println(distance.getTotalDistance(population.getRoutes().get(i)));
               // System.out.println(distance.getTotalDistance(bestRoute));
                    //if(distance.getTotalDistance(population.getRoutes().get(i)) < distance.getTotalDistance(bestRoute)){
                    //    System.out.println("better route");
                    //    bestRoute = null;
                    //    //bestRoute=population.getRoutes().get(i);
                   //  }

                System.out.print(population.getRoutes().get(i).getCityNames());

                double fitness = (1/distance.getTotalDistance(population.getRoutes().get(i)))*10000;


               // System.out.print("    "+fitness);
                //System.out.println("    "+distance.getTotalDistance(population.getRoutes().get(i)));
                System.out.println("    "+String.format(" %.2f",distance.getTotalDistance(population.getRoutes().get(i))));
                //population.getRoutes().get(i))
                //+String.format(" %.2f",distance.getTotalDistance(currentRoute)))
                //System.out.println("Best Route Found so far: " + bestRoute.getCityNames());

            }
        }

        System.out.println();
        System.out.println();

       // System.out.println("Best Route Found so far: " + bestRoute.getCityNames());
       // System.out.println("    "+distance.getTotalDistance(bestRoute));

    }








    private ArrayList<City> newCities = new ArrayList<City>(Arrays.asList(
            new City("1", 36.902328, 5.865524),
            new City("2", 32.268564, -9.772170),
            new City("3", 38.430958, 4.616171) ,
            new City("4", 17.085076, -13.717743),
            new City("5", -2.378420, -12.912119),
            new City("6", 35.229322, -11.952753),
            new City("7", 8.734037, -37.925268),
            new City("8", 18.213856,30.067160),
            new City("9", -17.367236, 14.994152),
            new City("10", -16.055342, 36.092639),
            new City("11", 36.893950, -4.489910),
            new City("12", 11.354847, 37.879510),
            new City("13", 39.040952, 6.060413),
            new City("14", -7.949413 , 3.900184) ,
            new City("15", -35.113496 , -9.111273),
            new City("16", 3.920608, 28.751273),
            new City("17", 24.411612  ,26.044301),
            new City("18", 34.976862, 5.514005),
            new City("19", 12.745841, 28.464310 ),
            new City("20", 23.195509, 25.060627),
            new City("21", 9.107489, 16.970196),
            new City("22", 39.281107, 4.053225) ,
            new City("23", 9.510174, -15.065098),
            new City("24", 39.103806, -2.333589),
            new City("25", 3.162014, 37.917558),
            new City("26", -6.567441, -7.311222 ),
            new City("27", 8.555034,38.095727),
            new City("28", 0.938855, 11.778574),
            new City("29", -36.020211, -4.791656),
            new City("30", -3.040275, 20.917682)

    ));





    public GeneticAlgorithmApplication() { this.newCities = newCities; }

    public ArrayList<City> getInitialRoute() { return newCities; }

    public Population evolve(Population population) { return mutatePopulation(crossoverPopulation(population)); }
    Population crossoverPopulation(Population population) {
        Population crossoverPopulation = new Population(newCities.size(),newCities);
        IntStream.range(0, eliteRouteNumber).forEach(x -> crossoverPopulation.getRoutes().set(x, population.getRoutes().get(x)));
        IntStream.range(eliteRouteNumber, crossoverPopulation.getRoutes().size()).forEach(x -> {
            Route route1 = selectTournamentPopulation(population).getRoutes().get(0);
            Route route2 = selectTournamentPopulation(population).getRoutes().get(0);
            crossoverPopulation.getRoutes().set(x, crossoverRoute(route1, route2));
        });
        return crossoverPopulation;
    }
    Population mutatePopulation(Population population) {
        population.getRoutes().stream().filter(x -> population.getRoutes().indexOf(x) >= eliteRouteNumber).forEach(x -> mutateRoute(x));
        return population;
    }
    //an example crossover of route1 and route2
    //         route1            : [New York, San Francisco, Houston, Chicago, Boston, Austin, Seattle, Denver, Dallas, Los Angeles]
    //         route2            : [Los Angeles, Seattle, Austin, Boston, Denver, New York, Houston, Dallas, San Francisco, Chicago]
    //intermediate crossoverRoute: [New York, San Francisco, Houston, Chicago, Boston, null, null, null, null, null]
    //   final     crossoverRoute: [New York, San Francisco, Houston, Chicago, Boston, Los Angeles, Seattle, Austin, Denver, Dallas]
    Route crossoverRoute(Route route1, Route route2) {
        Route crossoverRoute = new Route(newCities);
        Route tempRoute1 = route1;
        Route tempRoute2 = route2;
        if (Math.random() < 0.5) {
            tempRoute1 = route2;
            tempRoute2 = route1;
        }
        for (int x = 0; x < crossoverRoute.getCities().size()/2; x++)
            crossoverRoute.getCities().set(x, tempRoute1.getCities().get(x));
        return fillNullsInCrossoverRoute(crossoverRoute, tempRoute2);
    }
    //crossoverRoute: [Seattle, Houston, Denver, Los Angeles, San Francisco, null, null, null, null, null]
    //   route      : [Los Angeles, Chicago, Austin, Houston, Denver, San Francisco, Seattle, Boston, New York, Dallas]
    //crossoverRoute: [Seattle, Houston, Denver, Los Angeles, San Francisco, Chicago, Austin, Boston, New York, Dallas]
    private Route fillNullsInCrossoverRoute(Route crossoverRoute, Route route) {
        route.getCities().stream().filter(x -> !crossoverRoute.getCities().contains(x)).forEach(cityX -> {
            for (int y = 0; y < route.getCities().size(); y++) {
                if (crossoverRoute.getCities().get(y) == null) {
                    crossoverRoute.getCities().set(y, cityX);
                    break;
                }
            }
        });
        return crossoverRoute;
    }
    //an example route mutation
    //original route: [Boston, Denver, Los Angeles, Austin, New York, Seattle, Chicago, San Francisco, Dallas, Houston]
    // mutated route: [Boston, Denver, New York, Austin, Los Angeles, Seattle, San Francisco, Chicago, Dallas, Houston]
    Route mutateRoute(Route route) {
        route.getCities().stream().filter(x -> Math.random() < mutationRate).forEach(cityX -> {
            int y = (int) (route.getCities().size() * Math.random());
            City cityY = route.getCities().get(y);
            route.getCities().set(route.getCities().indexOf(cityX), cityY);
            route.getCities().set(y, cityX);
        });
        return route;
    }
    Population selectTournamentPopulation(Population population) {
        Population population1=new Population(tournamentSize,newCities);
        Population tournamentPopulation = new Population(tournamentSize, newCities);
        IntStream.range(0, tournamentSize).forEach(x -> tournamentPopulation.getRoutes().set(
                x, population.getRoutes().get((int) (Math.random() * population.getRoutes().size()))));
        //tournamentPopulation.sortRoutesByFitness();
        return tournamentPopulation;
    }


}
